import axios from "axios";

const API = axios.create({
    url:process.env.REACT_APP_API_URL
})