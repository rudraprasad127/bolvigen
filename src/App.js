import React,{Fragment} from 'react'
import './App.css';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Login from './Components/Auth/Login'
import Register from "./Components/Register/Register";
import LandingPage from "./Components/LandingPage";
import Footer from "./Components/Footer/Footer";
import Nav from "./Components/Nav/Nav";
import Nav2 from "./Components/LandingPage/Nav";
import Sidemenu from "./Components/SideMenu/Sidemenu";
import SideMenuVendor from './Components/SideMenuVendor/Sidemenu'
import Dashboard from "./Components/Dashboard";
import ProductList from './Components/ProductList';
import ProductDetails from './Components/ProductDetails'

function App() {
  return (
      <BrowserRouter>
        <Routes>
            <Route path='/' exact element={
                <Fragment>
                    <Nav2/>
                    <LandingPage />
                    <Footer />
                </Fragment>
            }/>
            <Route path='/login' exact element={
                <Fragment>
                    <Nav/>
                    <LandingPage />
                    <Login />
                </Fragment>
            }/>
            <Route path='/resetpassword/:token' exact element={
                <Fragment>
                    <Nav/>
                    <LandingPage />
                    <Login />
                </Fragment>
            }/>
            <Route path='/register/:type' element={
                <>
                    <Nav/>
                    <LandingPage />
                    <Register />
                </>
            }/>
            <Route path='/admin' element={
                <>
                    <Sidemenu
                        Component={Dashboard}
                        name='Dashboard'
                    />
                </>
            }/>
            <Route path='/vendor' element={
                <>
                    <SideMenuVendor
                        Component={Dashboard}
                        name='Dashboard'
                    />
                </>
            }/>
            <Route path='/list' exact element={
                <Fragment>
                    <Nav/>
                    <ProductList />
                    <Footer />
                </Fragment>
            }/>
            <Route path='/list/details' exact element={
                <Fragment>
                    <Nav/>
                    <ProductDetails />
                    <Footer />
                </Fragment>
            }/>
        </Routes>
      </BrowserRouter>
  );
}

export default App;
