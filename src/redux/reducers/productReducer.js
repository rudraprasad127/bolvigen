import {ActionTypes} from "../contants/action-types";

const initialState = {
    auth:{},
    currentPage:'',

}

export const authReducer = (state = initialState, {type,payload}) =>{
    switch (type){
        case ActionTypes.SET_DATA:
            return {...state,data:[...state,payload]}
        default:
            return state
    }
}