import React from 'react';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import completedOrders from './assets/completedOrders.png'
import filedOrders from './assets/filedOrders.png'
import pendingOrders from './assets/pendingOrders.png'
import totalOrders from './assets/totalOrders.png'

const OrderDashboard = () => {
    return (
        <Grid container xs={12} spacing={2} sx={{width:'100%',display:'flex',justifyContent:'space-between'}} >
            <Grid item xs={3} >
                <Paper elevation={0} sx={{height:'150px',width:'100%'}}>
                    <Typography variant="h6" component="div" color="#6a3a8e" sx={{fontWeight:"bold",padding:'20px'}}>
                        Orders Completed
                    </Typography>
                    <Box sx={{fontWeight:"bold",padding:'20px',display:'flex',alignItems:'center'}}>
                        <img width='40px' src={completedOrders} alt='Completed Orders'/>
                        <Typography variant="h4" component="div" color="#6a3a8e" sx={{fontWeight:"bold",marginLeft:'20px'}}>
                            500
                        </Typography>
                    </Box>
                </Paper>
            </Grid>
            <Grid item xs={3} >
                <Paper elevation={0} sx={{height:'150px',width:'100%'}}>
                    <Typography variant="h6" component="div" color="#6a3a8e" sx={{fontWeight:"bold",padding:'20px'}}>
                        Order Pending
                    </Typography>
                    <Box sx={{fontWeight:"bold",padding:'20px',display:'flex',alignItems:'center'}}>
                        <img width='40px' src={pendingOrders} alt='Completed Orders'/>
                        <Typography variant="h4" component="div" color="#6a3a8e" sx={{fontWeight:"bold",marginLeft:'20px'}}>
                            30
                        </Typography>
                    </Box>
                </Paper>
            </Grid>
            <Grid item xs={3} >
                <Paper elevation={0} sx={{height:'150px',width:'100%'}}>
                    <Typography variant="h6" component="div" color="#6a3a8e" sx={{fontWeight:"bold",padding:'20px'}}>
                        Orders Failed
                    </Typography>
                    <Box sx={{fontWeight:"bold",padding:'20px',display:'flex',alignItems:'center'}}>
                        <img width='40px' src={filedOrders} alt='Completed Orders'/>
                        <Typography variant="h4" component="div" color="#6a3a8e" sx={{fontWeight:"bold",marginLeft:'20px'}}>
                            70
                        </Typography>
                    </Box>
                </Paper>
            </Grid>
            <Grid item xs={3} >
                <Paper elevation={0} sx={{height:'150px',width:'100%'}}>
                    <Typography variant="h6" component="div" color="#6a3a8e" sx={{fontWeight:"bold",padding:'20px'}}>
                        Total Orders
                    </Typography>
                    <Box sx={{fontWeight:"bold",padding:'20px',display:'flex',alignItems:'center'}}>
                        <img width='40px' src={totalOrders} alt='Completed Orders'/>
                        <Typography variant="h4" component="div" color="#6a3a8e" sx={{fontWeight:"bold",marginLeft:'20px'}}>
                            1K
                        </Typography>
                    </Box>
                </Paper>
            </Grid>
        </Grid>
    );
};

export default OrderDashboard;
