import React from 'react';
import { Bar } from 'react-chartjs-2';
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import CalendarTodayIcon from '@mui/icons-material/CalendarToday';
import Typography from "@mui/material/Typography";
import pendingOrders from "./assets/pendingOrders.png";
import Paper from "@mui/material/Paper";
const data = {
    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun','Jul', 'Aug', 'Sep','Oct', 'Nov', 'Dec'],
    datasets: [
        {
            label: 'Transection',
            data: [25000, 12000, 35000, 30000, 36000, 38000,35000,31000,10000,8000,38000,41000,37000],
            backgroundColor: [
                '#d6b236',
            ],
            borderWidth: 1,
        },
    ],
};

const options = {
    scales: {
        yAxes: [
            {
                ticks: {
                    beginAtZero: true,
                },
            },
        ],
    },
};

const VerticalBar = () => {
    const [year, setYear] = React.useState('');

    const handleChange = (event) => {
        setYear(event.target.value);
    };
    return(
        <>
            <Grid container xs={12} spacing={3} marginTop='40px'>
                <Box sx={{width:'100%',display:'flex',justifyContent:'flex-end'}}>
                    <FormControl sx={{width:'120px',borderRadius:'10px'}}>
                        <InputLabel id="demo-simple-select-label" sx={{display:'flex'}}><CalendarTodayIcon fontSize='small' sx={{marginRight:'10px',backgroundColor:'white'}}/> Years </InputLabel>
                        <Select
                            id="demo-simple-select"
                            value={year}
                            onChange={handleChange}
                        >
                            <MenuItem value={'2020'}>2020</MenuItem>
                            <MenuItem value={'2021'}>2021</MenuItem>
                        </Select>
                    </FormControl>
                </Box>
                <Grid item xs={9} sx={{display:'flex',justifyContent:'center'}}>
                    <Bar data={data} options={options} style={{width:'95%',height:'100%'}} />
                </Grid>
                <Grid item xs={3} sx={{width:'90%',display:'flex',flexDirection:'column',justifyContent:'space-between'}}>
                    <Paper elevation={0} sx={{height:'160px',width:'100%',margin:'10px'}}>
                        <Typography variant="h6" component="div" color="#6a3a8e" sx={{fontWeight:"bold",padding:'20px'}}>
                            Yearly Transactions
                        </Typography>
                        <Box sx={{fontWeight:"bold",display:'flex',alignItems:'center'}}>
                            <Typography variant="h3" component="div" color="#6a3a8e" sx={{fontWeight:"bold",marginLeft:'20px'}}>
                               $ 500K
                            </Typography>
                        </Box>
                    </Paper>
                    <Paper elevation={0} sx={{height:'160px',width:'100%',margin:'10px'}}>
                        <Typography variant="h6" component="div" color="#6a3a8e" sx={{fontWeight:"bold",padding:'20px'}}>
                            Yearly Order
                        </Typography>
                        <Box sx={{fontWeight:"bold",display:'flex',alignItems:'center'}}>
                            <Typography variant="h3" component="div" color="#6a3a8e" sx={{fontWeight:"bold",marginLeft:'20px'}}>
                                56
                            </Typography>
                        </Box>
                    </Paper>
                    <Paper elevation={0} sx={{height:'160px',width:'100%',margin:'10px'}}>
                        <Typography variant="h6" component="div" color="#6a3a8e" sx={{fontWeight:"bold",padding:'20px'}}>
                            Total Transactions
                        </Typography>
                        <Box sx={{fontWeight:"bold",display:'flex',alignItems:'center'}}>
                            <Typography variant="h3" component="div" color="#6a3a8e" sx={{fontWeight:"bold",marginLeft:'20px'}}>
                                $ 25K
                            </Typography>
                        </Box>
                    </Paper>
                </Grid>
            </Grid>
        </>
    );
}


export default VerticalBar;