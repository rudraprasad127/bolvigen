import React,{Fragment} from 'react';
import OrderDashboard from "./OrderDashboard";
import OrderChart from "./OrderChart";

const Dashboard = () => {
    return (
        <Fragment>
            <OrderDashboard/>
            <OrderChart/>
        </Fragment>
    );
};

export default Dashboard;
