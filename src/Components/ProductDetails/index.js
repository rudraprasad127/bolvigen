import React from 'react';
import Container from '@mui/material/Container';
// import Slider from "./Slider";
import { Grid, Button, Typography, Divider } from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';

const ProductDetails = (props) => {
    return (
        <Grid>
            <Grid sx={{ background: '#fff', display : 'flex', padding : '10px 0'}}>
                <Container >
                    <Grid container xs="12" >
                        <Grid item xs="12">
                            <Button  size="large" sx={{ color : '#472264', fontWeight : 'bold'}}  startIcon={<ArrowBackIcon  className="back-btn"  />}>
                            Brangus
                            </Button>
                        </Grid>
                    </Grid>
                </Container>
            </Grid>
            <Grid sx={{ background: '#EAEAEA' , padding : '45px 25px'}}>
                <Container>
                    <Grid container xs="12">
                        <Grid item xs="5">
                            {/*<Slider></Slider>*/}
                        </Grid>
                        <Grid item xs="7">
                            <Grid item xs="12"><Typography variant="h4" sx={{ color : '#693A8E', paddingBottom : '15px'}}>Saint Igantius 56- Mr.Morris</Typography></Grid>
                            <Grid item xs="12"><Typography variant="h6" sx={{ color : '#D6B336'}}>$ 41,500.00</Typography></Grid>
                            <Grid item xs="12"><Typography variant="subtitle1" sx={{ color : '#693A8E'}}>Including Tax</Typography></Grid>
                            <Divider sx={{ margin: '25px 0'}}></Divider>
                            <Grid container>
                                <Grid item xs="6" className="single-page-list">
                                    <Typography variant="h6" sx={{ color : '#693A8E'}}>PARENT DATA</Typography>
                                    <Typography variant="subtitle2"><strong>Animal Name</strong> : TEST</Typography>
                                    <Typography variant="subtitle2"><strong>Nick Name</strong> : TEST</Typography>
                                    <Typography variant="subtitle2"><strong>Gender</strong> : Female</Typography>
                                    <Typography variant="subtitle2"><strong>Gender</strong> : Female</Typography>
                                </Grid>
                                <Grid item xs="6">
                                    <Typography variant="h6" sx={{ color : '#693A8E'}}>MOTHER DATA</Typography>
                                    <Typography variant="subtitle2"><strong>Animal Name</strong> : TEST</Typography>
                                    <Typography variant="subtitle2"><strong>Nick Name</strong> : TEST</Typography>
                                    <Typography variant="subtitle2"><strong>Gender</strong> : Female</Typography>
                                    <Typography variant="subtitle2"><strong>Gender</strong> : Female</Typography>
                                </Grid>
                            </Grid>

                        </Grid>
                    </Grid>
                </Container>
            </Grid>
            
        </Grid>
    );
};

export default ProductDetails;