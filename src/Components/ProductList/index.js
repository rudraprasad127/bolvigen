import React, {Fragment} from 'react';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Typography from "@mui/material/Typography";
import {Button} from "@mui/material";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import './style.css'
import Product from './Product';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';

const ProductList = (props) => {
    return (
        <Fragment >
            <Grid sx={{ background: '#fff', display : 'flex', padding : '10px 0'}}>
                <Container >
                    <Grid container xs="12" >
                        <Grid item xs="6">
                            <Button  size="large" sx={{ color : '#472264', fontWeight : 'bold'}}  startIcon={<ArrowBackIcon  className="back-btn"  />}>
                            Brangus
                            </Button>
                        </Grid>
                        <Grid item xs="6" >
                            <Typography style={{ alignItems: 'center', height: '100%', justifyContent: 'flex-end', width: '100%', display : 'flex'}}>Showing 1 - 12 of 40 item (s)</Typography>
                        </Grid>
                    </Grid>
                </Container>
            </Grid>
            <Grid sx={{ background: '#EAEAEA' , padding : '25px'}}>
                <Container>
                    <Grid container justifyContent="left" alignItems="center" spacing={{ xs: 1, sm: 2, md: 3 }} >
                        <Product />
                        <Product />
                        <Product />
                        <Product />
                        <Product />
                    </Grid>
                </Container>
            </Grid>
            <Grid sx={{ background: '#EAEAEA', display : 'flex', padding : '10px 25px'}}>
                <Container >
                    <Grid container xs="12" >
                        <Grid item xs="12" md="6" sx={{ alignItems: { paddingBottom : '10px',xs : 'center',md: 'end'} }}> 
                        <FormControl size="small"  sx={{  m: 1, minWidth: 140, background:"#fff" }} >
                            <InputLabel id="demo-simple-select-helper-label">Show</InputLabel>
                            <Select
                            labelId="demo-simple-select-helper-label"
                            id="demo-simple-select-helper"
                            label="Show"
                            >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            <MenuItem value={10}>10</MenuItem>
                            <MenuItem value={20}>20</MenuItem>
                            <MenuItem value={30}>30</MenuItem>
                            </Select>
                        </FormControl>
                        </Grid>
                        <Grid item xs="12" md="6" display="flex" alignItems="center" justifyContent="center">
                        <Stack spacing={2} width="100%" display="flex" sx={{ alignItems: { xs : 'left',md: 'end'} }}  justifyContent="center">
                            <Pagination count={5} variant="outlined" shape="rounded" />
                        </Stack>
                        </Grid>
                    </Grid>
                </Container>
            </Grid>     
        </Fragment>
    );
};

export default ProductList;