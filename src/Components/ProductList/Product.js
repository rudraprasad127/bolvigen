import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import CardIMG from './assets/Image1.png'
import './style.css'
import TextField from '@mui/material/TextField';
import ButtonGroup from '@mui/material/ButtonGroup';
import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { IconButton } from '@mui/material';

export default function MediaCard() {
  return (
    <Grid item xs={12} sm={6} md={4} lg={3} sx={{ marginBottom : '15px'}}>
        <Card sx={{ maxWidth: 345, paddingBottom : '5px' }} >
        <CardMedia
            component="img"
            height="auto"
            image={CardIMG}
            alt="green iguana"
        />
        <CardContent sx={{ padding : '10px'}}>
            <Typography className="color-main" gutterBottom  component="div" variant="h6" sx={{ fontWeight : '600', paddingTop : '15px'}}>
            Saint Ignatius 56-Mr.Angus
            </Typography>
            <Typography className="color-main" gutterBottom variant="h5" component="div" sx={{ fontWeight : 'bolder'}}>
            $ 48,975.75
            </Typography>
        </CardContent>
        <CardActions sx={{ padding : '10px'}}>
        <Grid className="cart-items-box" xs={4}>
            <Typography sx={{ paddingLeft : '5px'}} variant="h5">0</Typography>
            <Grid className="button-grp">
                <IconButton className="btns-inc-dec" ><ExpandLessIcon /></IconButton>
                <IconButton className="btns-inc-dec" ><KeyboardArrowDownIcon  /></IconButton>
            </Grid>
        </Grid>
        <Grid xs={8}>
            <Button variant="contained" className="add-cart-btn" sx={{ width : '100%'}}>Add to cart</Button>
        </Grid>
        </CardActions>
        </Card>
    </Grid>
  );
}
