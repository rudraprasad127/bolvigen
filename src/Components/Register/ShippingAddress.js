import React, {useState} from 'react';
import Typography from "@mui/material/Typography";
import {Grid} from "@mui/material";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import {ColorButton} from "./Style";
import {updateShippingAdderess} from "./api";
import Alert from "@mui/material/Alert";
import CircularProgress from "@mui/material/CircularProgress";
const initialState = {
    address_line1: "",
    address_line2: "",
    country: "",
    state: "",
    zipcode: ""
}
const RegisterForm = ({setOpenSuccessModel,setOpenRegisterModel}) => {
    const [values, setValues] = React.useState(initialState);
    const [error, setError] = useState('')
    const [isLoading, setIsLoading] = useState(false);
    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleSubmit = async () => {
        setIsLoading(true)
        const profile = JSON.parse(localStorage.getItem('profile'))
        const token = localStorage.getItem('authToken')
        try{
            if(values.address_line1 && values.address_line2 && values.country && values.state && values.zipcode){
                const finalData = {
                    name:profile.full_name,
                    address_attributes:values
                }
                const {data} = await updateShippingAdderess(finalData,token)
                if(data.errors){
                    setError(data.errors)
                }else{
                    setOpenRegisterModel(false)
                    setOpenSuccessModel(true)
                }
            }else{
                setError([
                    {
                        message:'Please Enter All Fields'
                    }
                ])
            }
        } catch (e) {
            setError([
                {
                    message:'Something Went Wrong'
                }
            ])
        }
        setIsLoading(false)
    }

    return (
        <Grid item xs={12} className='registerMainDiv'>
            <Box className='registerFormFlex'>
                <Typography variant="h6" component="div" color="purple" sx={{fontWeight:"bold"}}>
                    Shipping Address
                </Typography>
                <Typography variant="subtitle1" component="div" color="purple">
                    Fields marks with asterisk (*) are Required.
                </Typography>
                <Grid item xs={12} sx={{marginTop:"20px"}}>
                    {
                        error ?
                            error.map((e,index)=>{
                                return(
                                    <Alert key={index} severity="error" sx={{width:'82%',alignSelf:'center'}}>
                                        {e.message}
                                    </Alert>
                                )
                            })
                            : null
                    }
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            Address Line 1
                            <Typography variant="subtitle1" color='secondary' component="span" sx={{fontWeight:"bold"}}>
                                *
                            </Typography>
                        </Typography>
                        <TextField block placeholder="Enter your full name" size='small' sx={{width:"90%"}} value={values.address_line1} onChange={handleChange('address_line1')}/>
                    </Box>
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            Address Line 2
                            <Typography variant="subtitle1" color='secondary' component="span" sx={{fontWeight:"bold"}}>
                                *
                            </Typography>
                        </Typography>
                        <TextField block placeholder="Enter your email address" size='small' sx={{width:"90%"}} value={values.address_line2} onChange={handleChange('address_line2')}/>
                    </Box>
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            Country
                        </Typography>
                        <TextField block placeholder="Enter your Country" size='small' sx={{width:"90%"}}  onChange={handleChange('country')}/>
                        {/*<FormControl sx={{width:'90%'}}>*/}
                        {/*    <Select*/}
                        {/*        size='small'*/}
                        {/*        value={values.country}*/}
                        {/*        onChange={handleChange('address_attributes[country]')}*/}
                        {/*        displayEmpty*/}
                        {/*        inputProps={{ 'aria-label': 'Without label' }}*/}
                        {/*    >*/}
                        {/*        <MenuItem value="">*/}
                        {/*            <em>None</em>*/}
                        {/*        </MenuItem>*/}
                        {/*        <MenuItem value='India'>India</MenuItem>*/}
                        {/*    </Select>*/}
                        {/*</FormControl>*/}
                    </Box>
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            State
                        </Typography>
                        <TextField block placeholder="Enter your State" size='small' sx={{width:"90%"}}  onChange={handleChange('state')}/>

                        {/*<FormControl sx={{width:'90%'}}>*/}
                        {/*    <Select*/}
                        {/*        size='small'*/}
                        {/*        value={values.state}*/}
                        {/*        onChange={handleChange('address_attributes[state]')}*/}
                        {/*        displayEmpty*/}
                        {/*        inputProps={{ 'aria-label': 'Without label' }}*/}
                        {/*    >*/}
                        {/*        <MenuItem value="">*/}
                        {/*            <em>None</em>*/}
                        {/*        </MenuItem>*/}
                        {/*        <MenuItem value={10}>States</MenuItem>*/}
                        {/*    </Select>*/}
                        {/*</FormControl>*/}
                    </Box>
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            Zipcode
                        </Typography>
                        <TextField block placeholder="Enter your email address" size='small' sx={{width:"90%"}} onChange={handleChange('zipcode')}/>
                    </Box>
                    <Box>
                        <ColorButton variant="contained" color="secondary" sx={{width:"90%",height:'50px'}} onClick={handleSubmit}>{isLoading ? <CircularProgress /> : "Save"}</ColorButton>
                    </Box>
                </Grid>
            </Box>
        </Grid>
    );
};

export default RegisterForm;
