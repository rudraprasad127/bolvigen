import React, {useState} from 'react';
import Typography from "@mui/material/Typography";
import {Grid} from "@mui/material";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import {ColorButton, registerStpes, registerStpesLine} from "./Style";
import {updateBankDetails} from "./api";
import CircularProgress from "@mui/material/CircularProgress";
import Alert from "@mui/material/Alert";

const initialState = {
    full_name: "",
    bank_name: "",
    bank_country: "",
    account_number: "",
    account_number_confirmation: "",
    routing_number: '',
    swift_key: ""
}
const RegisterForm = ({setOpenRegisterModel,setOpenSuccessModel}) => {
    const [values, setValues] = React.useState(initialState);
    const [error, setError] = useState('')
    const [isLoading, setIsLoading] = useState(false)
    const token = window.localStorage.getItem('authToken')
    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleSubmit = async () => {
        setIsLoading(true)
        if(values.full_name && values.bank_name && values.bank_country && values.account_number && values.account_number_confirmation && values.routing_number && values.swift_key){
            try {
                const {data} = await updateBankDetails(values,token)
                console.log(data)
                if(data.errors){
                    setError(data.errors)
                }else{
                    setOpenRegisterModel(false)
                    setOpenSuccessModel(true)
                }
            }catch (e) {
                setError([
                    {
                        message:'Something Went Wrong'
                    }
                ])
            }
        }else{
            setError([
                {
                    message:'Please Check and Fill Blank fields'
                }
            ])
        }
        setIsLoading(false)
    }

    return (
        <Grid item xs={12} className='registerMainDiv'>
            <Box className='registerFormFlex'>
                <Typography variant="h6" component="div" color="purple" sx={{fontWeight:"bold"}}>
                    Vendor Registration in only 3 Steps
                </Typography>
                <Typography variant="subtitle1" component="div" color="purple">
                    provide your firm bank details
                </Typography>
                <Box sx={{display: 'flex',flexDirection:'row',alignItems:'center',margin:'10px 0px'}}>
                    <Box sx={registerStpes} style={{backgroundColor:'purple'}}>

                    </Box>
                    <Box sx={registerStpesLine} style={{backgroundColor:'purple'}}>

                    </Box>
                    <Box sx={registerStpes} style={{backgroundColor:'purple'}}>

                    </Box>
                    <Box sx={registerStpesLine} style={{backgroundColor:'purple'}}>

                    </Box>
                    <Box sx={registerStpes} style={{backgroundColor:'purple'}}>

                    </Box>
                </Box>
                <Grid item xs={12} sx={{marginTop:"5px"}}>
                    {
                        error ?
                            error.map((e,index)=>{
                                return(
                                    <Alert key={index} severity="error" sx={{width:'82%',alignSelf:'center'}}>
                                        {e.message}
                                    </Alert>
                                )
                            })
                            : null

                    }
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            Full Name
                        </Typography>
                        <TextField block placeholder="Enter your full name" size='small' sx={{width:"90%"}} onChange={handleChange('full_name')}/>
                    </Box>
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            Bank Name
                        </Typography>
                        <TextField block placeholder="Enter bank name" size='small' sx={{width:"90%"}} onChange={handleChange('bank_name')}/>
                    </Box>
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            Bank Country
                        </Typography>
                        <TextField block placeholder="Enter your Bank Country" size='small' sx={{width:"90%"}}  onChange={handleChange('bank_country')}/>
                        {/*<FormControl sx={{width:'90%'}}>*/}
                        {/*    <Select*/}
                        {/*        size='small'*/}
                        {/*        value={values.country}*/}
                        {/*        onChange={handleChange('address_attributes[country]')}*/}
                        {/*        displayEmpty*/}
                        {/*        inputProps={{ 'aria-label': 'Without label' }}*/}
                        {/*    >*/}
                        {/*        <MenuItem value="">*/}
                        {/*            <em>None</em>*/}
                        {/*        </MenuItem>*/}
                        {/*        <MenuItem value='India'>India</MenuItem>*/}
                        {/*    </Select>*/}
                        {/*</FormControl>*/}
                    </Box>
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            Account Number
                        </Typography>
                        <TextField block placeholder="Enter Account Number" size='small' sx={{width:"90%"}}  onChange={handleChange('account_number')}/>
                    </Box>
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            Confirm Account Number
                        </Typography>
                        <TextField block placeholder="Confirm Account Number" size='small' sx={{width:"90%"}}  onChange={handleChange('account_number_confirmation')}/>
                    </Box>
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            Routing Number
                        </Typography>
                        <TextField block placeholder="Enter Routing Number" size='small' sx={{width:"90%"}}  onChange={handleChange('routing_number')}/>
                    </Box>
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            SWIFT Key
                        </Typography>
                        <TextField block placeholder="Enter SWIFT Key" size='small' sx={{width:"90%"}} onChange={handleChange('swift_key')}/>
                    </Box>
                    <Box>
                        <ColorButton variant="contained" color="secondary" sx={{width:"90%"}} onClick={handleSubmit}>{isLoading ? <CircularProgress /> : "Submit"}</ColorButton>
                    </Box>
                </Grid>
            </Box>
        </Grid>
    );
};

export default RegisterForm;
