import axios from "axios";

const API = axios.create({
    baseURL:process.env.REACT_APP_API_URL,
})

const geolocationAPI = axios.create({
    baseURL:'https://api.countrystatecity.in/v1/'
})

export const register = async (loginData) => {
    try{
        return await API.post(`/account_block/accounts`,loginData, {
            headers:{
                'Access-Control-Allow-Origin': "*",
                'Content-Type':'application/json'
            }
        })
    } catch (e) {
        return  e.response
    }
}

export const registrationVendor = async (formData,token) => {
    try{
        return await API.post(`/bx_block_customisableuserprofiles/vendor_firms`,formData, {
            headers:{
                'token':token,
                'Access-Control-Allow-Origin': "*",
                'Content-Type':'application/json'
            }
        })
    } catch (e) {
        return  e.response
    }
}
export const updateShippingAdderess = async (formData,token) => {
    try{
        return await API.post(`bx_block_customisableuserprofiles/shipping_addresses`,formData, {
            headers:{
                'token':token,
                'Access-Control-Allow-Origin': "*",
                'Content-Type':'application/json'
            }
        })
    } catch (e) {
        return  e.response
    }
}

export const updateBankDetails = async (formData,token) => {
    try{
        return await API.post(`bx_block_customisableuserprofiles/bank_details`,formData, {
            headers:{
                'token':token,
                'Access-Control-Allow-Origin': "*",
                'Content-Type':'application/json'
            }
        })
    } catch (e) {
        return  e.response
    }
}

export const getCountries = async () => {
    try{
        return await geolocationAPI.get(`countries`,{
            headers:{
                'API_KEY':'NmZzNjFIbGdkVTd6cXNZSnpGTEVya3VZYmRTVnNLM1UzcDQ5MkF1TQ==',
                'X-CSCAPI-KEY': "NmZzNjFIbGdkVTd6cXNZSnpGTEVya3VZYmRTVnNLM1UzcDQ5MkF1TQ==",
                'Content-Type':'application/json'
            }
        })
    } catch (e) {
        return  e.response
    }
}

export const getStates = async (country) => {
    try{
        return await geolocationAPI.get(`countries/${country}/states`,{
            headers:{
                'API_KEY':'NmZzNjFIbGdkVTd6cXNZSnpGTEVya3VZYmRTVnNLM1UzcDQ5MkF1TQ==',
                'X-CSCAPI-KEY': "NmZzNjFIbGdkVTd6cXNZSnpGTEVya3VZYmRTVnNLM1UzcDQ5MkF1TQ==",
                'Content-Type':'application/json'
            }
        })
    } catch (e) {
        return  e.response
    }
}




