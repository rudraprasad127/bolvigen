import React, {useState} from 'react';
import Typography from "@mui/material/Typography";
import {Grid} from "@mui/material";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import FormControl from "@mui/material/FormControl";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputAdornment from "@mui/material/InputAdornment";
import IconButton from "@mui/material/IconButton";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Visibility from "@mui/icons-material/Visibility";
import Checkbox from "@mui/material/Checkbox";
import {ColorButton, flexBox, HtmlTooltip} from "./Style";
import {useNavigate,useParams} from 'react-router-dom'
import {register} from "./api";
import Alert from "@mui/material/Alert";
import CircularProgress from "@mui/material/CircularProgress";
const initialState = {
    full_name: "",
    email:"",
    full_phone_number: "",
    password: "",
    password_confirmation: "",
    user_role: ""
}

const RegisterForm = ({setFormType, setOpenSuccessModel,setOpenRegisterModel}) => {
    const {type} = useParams()
    const navigate = useNavigate()
    const [values, setValues] = React.useState(initialState)
    const [error, setError] = useState('')
    const [termsCheckBox, setTermsCheckBox] = React.useState(false);
    const [passwordVisiblity, setPasswordVisiblity] = React.useState({
        showPassword1: false,
        showPassword2: false,
    });

    const [isLoading, setIsLoading] = useState(false);
    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const handleSubmit = async () => {
        setIsLoading(true)
        if(values.email && values.password && values.password_confirmation && values.full_name && values.full_phone_number){
            if(!termsCheckBox){
                setError([
                    {
                        message:'Please agree Terms & Conditions'
                    }
                ])
                setIsLoading(false)
                return
            }
            const finalData = {
                data:{
                    attributes:{
                        ...values,
                        user_role:type
                    }
                }
            }
            try {
                const {data} = await register(finalData)
                console.log(data)
                if(data.errors){
                    setError(data.errors)
                }else{
                    window.localStorage.setItem('authToken',data.meta.token)
                    window.localStorage.setItem('profile',JSON.stringify(data.data.attributes))
                    setFormType('success')
                }
            }catch (e) {
                setError([
                    {
                        message:'Something Went Wrong'
                    }
                ])
            }
        }else{
            setError([
                {
                    message:'Please Check and fill Empty Fields'
                }
            ])
        }
        setIsLoading(false)
    }

    return (
        <Grid item xs={12} className='registerMainDiv' sx={{ padding : '25px 0'}}>
            <Box className='registerFormFlex'>
                <Typography variant="h6" component="div" sx={{fontWeight:"bold", color: '#412360'}}>
                    Signup as a {type} !
                </Typography>
                <Typography variant="subtitle2" component="div" color="purple">
                    Fields marks with asterisk (*) are Required.
                </Typography>
                <Grid item xs={12} sx={{marginTop:"20px"}}>
                    {
                        error ?
                            error.map((e,index)=>{
                                return(
                                    <Alert key={index} severity="error" sx={{width:'82%',alignSelf:'center'}}>
                                        {e.message}
                                    </Alert>
                                )
                            })
                            : null

                    }
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            Name
                            <Typography variant="subtitle1" color='secondary' component="span" sx={{fontWeight:"bold"}}>
                                *
                            </Typography>
                        </Typography>
                        <TextField block placeholder="Enter your full name" size='small' sx={{width:"90%"}} value={values.full_name} onChange={handleChange('full_name')}/>
                    </Box>
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            Email
                            <Typography variant="subtitle1" color='secondary' component="span" sx={{fontWeight:"bold"}}>
                                *
                            </Typography>
                        </Typography>
                        <TextField block placeholder="Enter your email address" size='small' sx={{width:"90%"}} value={values.email} onChange={handleChange('email')}/>
                    </Box>
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            Mobile
                        </Typography>
                        <TextField
                            id="input-with-icon-textfield"
                            placeholder="Enter Phone"
                            sx={{ m: 0,width:"90%" }}
                            onChange={handleChange('full_phone_number')}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        +41 |
                                    </InputAdornment>
                                ),
                            }}
                            variant="outlined"
                            size="small"
                            value={values.full_phone_number}
                        />
                    </Box>
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold",marginBottom:'1'}}>
                            Password
                            <Typography variant="subtitle1" color='secondary' component="span" sx={{fontWeight:"bold"}}>
                                *
                            </Typography>
                        </Typography>
                        <HtmlTooltip
                            title={
                                <React.Fragment>
                                    <Typography variant="subtitle2" color="inherit">Password must have</Typography>
                                    <Typography variant="subtitle2" color="inherit">- Minimum one capital latter</Typography>
                                    <Typography variant="subtitle2" color="inherit">- Spacial Characters</Typography>
                                    <Typography variant="subtitle2" color="inherit">- Numbers</Typography>

                                </React.Fragment>
                            }
                        >
                            <FormControl sx={{ m: 0,width:"90%" }}>
                                <OutlinedInput
                                    id="outlined-adornment-password"
                                    type={passwordVisiblity.showPassword1 ? 'text' : 'password'}
                                    value={values.password}
                                    placeholder="Enter your password"
                                    onChange={handleChange('password')}
                                    size="small"
                                    endAdornment={
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={()=>{
                                                    setPasswordVisiblity({
                                                        ...passwordVisiblity,
                                                        showPassword1: !passwordVisiblity.showPassword1,
                                                    })
                                                }}
                                                onMouseDown={handleMouseDownPassword}
                                                edge="end"
                                            >
                                                {passwordVisiblity.showPassword1 ? <VisibilityOff/> : <Visibility/>}
                                            </IconButton>
                                        </InputAdornment>
                                    }
                                />
                            </FormControl>
                        </HtmlTooltip>

                    </Box>
                    <Box>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold",marginBottom:'1'}}>
                            Confirm Password
                            <Typography variant="subtitle1" color='secondary' component="span" sx={{fontWeight:"bold"}}>
                                *
                            </Typography>
                        </Typography>
                        <FormControl sx={{ m: 0,width:"90%" }}>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={passwordVisiblity.showPassword2 ? 'text' : 'password'}
                                value={values.password_confirmation}
                                placeholder="Enter your password"
                                onChange={handleChange('password_confirmation')}
                                size="small"
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={()=>{
                                                setPasswordVisiblity({
                                                    ...passwordVisiblity,
                                                    showPassword2: !passwordVisiblity.showPassword2,
                                                })
                                            }}
                                            onMouseDown={handleMouseDownPassword}
                                            edge="end"
                                        >
                                            {passwordVisiblity.showPassword2 ? <VisibilityOff/> : <Visibility/>}
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </Box>
                    <Box sx={flexBox}>
                        <Box sx={{display:'flex',flexDirection:'row', justifyContent:'center', alignContent:'center'}}>
                            <Checkbox sx={{padding:'0px'}} checked={termsCheckBox} onChange={() => setTermsCheckBox(!termsCheckBox)}/>
                            <Typography variant="body2" component="div" alignSelf='center' sx={{margin:'0px'}}>
                                I accept
                            </Typography>
                            <Typography variant="body2" component="div" color="#d8b434" alignSelf='center' marginLeft={0.5} sx={{cursor:'pointer'}}>
                                Terms & Condition
                            </Typography>
                            <Typography variant="body2" component="div" alignSelf='center' marginLeft={0.5}>
                                and
                            </Typography>
                            <Typography variant="body2" component="div" color="#d8b434" alignSelf='center' marginLeft={0.5} sx={{cursor:'pointer'}}>
                                Privacy Policy
                            </Typography>
                        </Box>
                    </Box>
                    <Box>
                        <ColorButton variant="contained" color="secondary" sx={{width:"90%"}} onClick={handleSubmit}>{isLoading ? <CircularProgress /> : "SIGNUP"}</ColorButton>
                    </Box>
                    <Box sx={{display:'flex',flexDirection:'row',alignContent:'space-between',justifyContent:'center',margin:'20px 0px'}}>
                        <Typography variant="body2" component="div" alignSelf='center' sx={{fontWeight:"bold"}}>
                            Already have an account?
                        </Typography>
                        <Typography variant="body2" component="div" color="#d8b434" alignSelf='center' marginLeft={1} sx={{fontWeight:"bold",cursor:'pointer'}} onClick={()=>navigate('/login')}>
                            Login
                        </Typography>
                    </Box>
                </Grid>
            </Box>
        </Grid>
    );
};

export default RegisterForm;
