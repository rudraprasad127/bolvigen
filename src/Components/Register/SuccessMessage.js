import React from 'react';
import Typography from "@mui/material/Typography";
import {Grid} from "@mui/material";
import Box from "@mui/material/Box";
import {ColorButton} from "./Style";
import {useParams} from 'react-router-dom'
import successCheck from './assets/successCheck.png'

const RegisterForm = ({setFormType}) => {
    const {type} = useParams()
    const handleNext= () => {
        if(type === 'buyer'){
            setFormType('shippingInfo')
        }else{
            setFormType('registerVendor1')
        }

    }
    return (
        <Grid item xs={12} className='registerMainDiv'>
            <Box className='registerFormFlex'>
                <Grid item xs={12} sx={{marginTop:"20px"}}>
                    <Box sx={{display:'flex',flexDirection:'column',alignItems:'center',width:'90%'}}>
                        <img src={successCheck} alt='success' width='100px' sx={{marginBottom:'30px'}}/>
                        <Typography variant="h4" component="div" color="purple" sx={{fontWeight:'bold',margin:'30px 0px'}}>
                            Link Send Successfully
                        </Typography>
                        <Typography variant="h6" component="span" color="purple" sx={{textAlign:'center',fontWeight:'bold'}}>
                            We have sent an email with a confirmation link to your email address. in order to complete the sign-up process, please click the confirmation link.
                        </Typography>
                    </Box>
                    <Box>
                        <ColorButton variant="contained" color="secondary" sx={{width:"90%",height:'50px',marginTop:'20px'}} onClick={handleNext}>Next</ColorButton>
                    </Box>
                </Grid>
            </Box>
        </Grid>
    );
};

export default RegisterForm;
