import React, {useState, useEffect} from 'react';
import Typography from "@mui/material/Typography";
import {Grid} from "@mui/material";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import FormControl from "@mui/material/FormControl";
import {ColorButton, registerStpes, registerStpesLine} from "./Style";
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import Alert from "@mui/material/Alert";
import {getCountries} from "./api";

const initialState = {
    name:'',
    address_line1:'',
    address_line2:'',
    country:'',
    state:'',
    zipcode:'',
}

const RegisterForm = ({setFormType,setVendorRegistration}) => {
    const [values, setValues] = React.useState(initialState);
    const [error, setError] = useState('')
    const [isLoading, setIsLoading] = useState(false);
    const [country, setCountry] = useState([]);
    const handleChange = (prop) => (event) => {
        console.log(event.target.value )
        setValues({ ...values, [prop]: event.target.value });
    };

    useEffect(() => {
        countryList()
    }, []);
    const countryList = async () => {
        const {data} = await getCountries()
        setCountry(data)
    }

    const handleSubmit = () => {
        if(values.name && values.address_line1 && values.address_line2 && values.country && values.state && values.zipcode){
            setVendorRegistration(values)
            setFormType('registerVendor2')
        }else{
            setError([
                {
                    message:'Please Fill Proper details'
                }
            ])
        }

    }

    return (
        <Grid item xs={12} className='registerMainDiv'>
            <Box className='registerFormFlex'>
                <Typography variant="h6" component="div" color="purple" sx={{fontWeight:"bold"}}>
                    Vendor Registration in only 3 Steps
                </Typography>
                <Typography variant="subtitle1" component="div" color="purple">
                    provide your firm details and documents
                </Typography>
                <Box sx={{display: 'flex',flexDirection:'row',alignItems:'center',margin:'20px 0px'}}>
                    <Box sx={registerStpes} style={{backgroundColor:'purple'}}>

                    </Box>
                    <Box sx={registerStpesLine} style={{backgroundColor:'#F0F0F0'}}>

                    </Box>
                    <Box sx={registerStpes} style={{backgroundColor:'#F0F0F0'}}>

                    </Box>
                    <Box sx={registerStpesLine} style={{backgroundColor:'#F0F0F0'}}>

                    </Box>
                    <Box sx={registerStpes} style={{backgroundColor:'#F0F0F0'}}>

                    </Box>
                </Box>
                <Grid item xs={12} sx={{marginTop:"10px"}}>
                    {
                        error ?
                            error.map((e,index)=>{
                                return(
                                    <Alert key={index} severity="error" sx={{width:'82%',alignSelf:'center'}}>
                                        {e.message}
                                    </Alert>
                                )
                            })
                            : null

                    }
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            Firm Name
                        </Typography>
                        <TextField block placeholder="Enter your full name" size='small' sx={{width:"90%"}} onChange={handleChange('name')}/>
                    </Box>
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            Address Line 1
                        </Typography>
                        <TextField block placeholder="Enter your email address" size='small' sx={{width:"90%"}} onChange={handleChange('address_line1')}/>
                    </Box>
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            Address Line 2
                        </Typography>
                        <TextField block placeholder="Enter your email address" size='small' sx={{width:"90%"}}  onChange={handleChange('address_line2')}/>
                    </Box>
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            Country
                        </Typography>
                        {/*<TextField block placeholder="Enter your Country" size='small' sx={{width:"90%"}}  onChange={handleChange('country')}/>*/}
                        <FormControl sx={{width:'90%'}}>
                            <Select
                                size='small'
                                value={values.country}
                                onChange={handleChange('country')}
                                displayEmpty
                                inputProps={{ 'aria-label': 'Without label' }}
                            >
                                <MenuItem value="">
                                    <em>None</em>
                                </MenuItem>
                                {
                                    country.map((c,index)=>{
                                        if(index <= 50) {
                                            return(
                                                <MenuItem value={`${c.name},${c.iso2}`}>{c.name}</MenuItem>
                                            )
                                        }else{
                                            return null
                                        }

                                    })
                                }
                                <MenuItem value='India'>India</MenuItem>
                            </Select>
                        </FormControl>
                    </Box>
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            State
                        </Typography>
                        <TextField block placeholder="Enter your State" size='small' sx={{width:"90%"}}  onChange={handleChange('state')}/>

                        {/*<FormControl sx={{width:'90%'}}>*/}
                        {/*    <Select*/}
                        {/*        size='small'*/}
                        {/*        value={values.state}*/}
                        {/*        onChange={handleChange('address_attributes[state]')}*/}
                        {/*        displayEmpty*/}
                        {/*        inputProps={{ 'aria-label': 'Without label' }}*/}
                        {/*    >*/}
                        {/*        <MenuItem value="">*/}
                        {/*            <em>None</em>*/}
                        {/*        </MenuItem>*/}
                        {/*        <MenuItem value={10}>States</MenuItem>*/}
                        {/*    </Select>*/}
                        {/*</FormControl>*/}
                    </Box>
                    <Box sx={{marginBottom:"15px"}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            Zipcode
                        </Typography>
                        <TextField block placeholder="Enter your email address" size='small' sx={{width:"90%"}} onChange={handleChange('zipcode')}/>
                    </Box>
                    <Box>
                        <ColorButton variant="contained" color="secondary" sx={{width:"90%"}} onClick={handleSubmit}>NEXT</ColorButton>
                    </Box>
                </Grid>
            </Box>
        </Grid>
    );
};

export default RegisterForm;
