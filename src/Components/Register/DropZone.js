import React, {useCallback} from 'react'
import {useDropzone} from 'react-dropzone'
import Typography from "@mui/material/Typography";
import UploadFileIcon from '@mui/icons-material/UploadFile';
export default function MyDropzone({setFile}) {
    const onDrop = useCallback(acceptedFiles => {
        setFile({
            registration_id : acceptedFiles[0]
        })
    }, [])
    const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop,multiple:false})

    return (
        <div {...getRootProps()} className='dropZone' >
            <input {...getInputProps()} />
            {
                isDragActive ?
                    <Typography variant="subtitle1" component="div" color="purple">
                        Drop the files here ...
                    </Typography> :
                    <div style={{display:'flex',flexDirection:'column',alignItems:'center'}}>
                        <UploadFileIcon fontSize='large' color='secondary'/>
                        <Typography variant="subtitle1" component="div" color="purple">
                            Upload ID
                        </Typography>
                    </div>
            }
        </div>
    )
}