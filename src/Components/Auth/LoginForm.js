import React,{useState} from 'react';
import Typography from "@mui/material/Typography";
import CircularProgress from '@mui/material/CircularProgress';
import {Grid} from "@mui/material";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import FormControl from "@mui/material/FormControl";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputAdornment from "@mui/material/InputAdornment";
import IconButton from "@mui/material/IconButton";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Visibility from "@mui/icons-material/Visibility";
import Alert from '@mui/material/Alert';
import Checkbox from "@mui/material/Checkbox";
import {flexBox, ColorButton} from "./Style";
import {login} from "./api";
import {useNavigate} from 'react-router-dom'

const LoginForm = ({setFormType,setOpenLoginModel}) => {
    const navigate = useNavigate()
    const [values, setValues] = useState({
        showPassword: false,
    });
    const [error, setError] = useState('')
    const [isLoading, setIsLoading] = useState(false);
    const handleClickShowPassword = () => {
        setValues({
            ...values,
            showPassword: !values.showPassword,
        });
    };
    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const handleSubmit = async () => {
        setIsLoading(true)
        if(values.email && values.password){
            const finalData = {
                data:{
                    attributes:values
                }
            }
            try {
                const {data} = await login(finalData)
                if(data.errors){
                    setError(data.errors)
                }else{
                    window.localStorage.setItem('authToken',data.meta.token)
                    window.localStorage.setItem('profile',JSON.stringify(data.data.attributes))
                    if(data.data.attributes.user_role === 'super_administrator' || data.data.attributes.user_role === 'administrator' ){
                        navigate('/admin')
                    }else if(data.data.attributes.user_role === 'buyer'){
                        navigate('/')
                    }else if(data.data.attributes.user_role === 'vendor'){
                        navigate('/vendor')
                    }
                    setOpenLoginModel(false)
                }
            }catch (e) {
                setError([
                    {
                        failed_login:'Something Went Wrong'
                    }
                ])
            }
        }else{
            setError([
                {
                    failed_login:'Please Enter Email and Password'
                }
            ])
        }
        setIsLoading(false)
    }

    return (
        <Grid item xs={12} className='loginMainDiv'>
            <Box className='loginFormFlex' style={{ padding : '25px 0 25px 0'}}>
                <Typography variant="h5" component="div" gutterBottom sx={{fontWeight:"bold", color:'#412360'}}>
                    Welcome !
                </Typography>
                <Typography variant="subtitle2" component="div" gutterBottom color="purple">
                    Login in to your account
                </Typography>

                <Grid item xs={12} sx={{marginTop:"15px"}}>
                    {
                        error ?
                        <Alert severity="error" sx={{width:'82%',alignSelf:'center'}}>
                            {error.map((e)=>{
                                return(
                                    e.failed_login
                                )
                            })}
                        </Alert> : null

                    }
                    <Box sx={{marginBottom:"15px",marginTop:'15px'}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            Email
                        </Typography>
                        <TextField block placeholder="Email" sx={{width:"90%"}} value={values.email} onChange={handleChange('email')}/>
                    </Box>
                    <Box>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold",marginBottom:'1'}}>
                            Password
                        </Typography>
                        <FormControl sx={{ m: 0,width:"90%" }}>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={values.showPassword ? 'text' : 'password'}
                                value={values.password}
                                placeholder="Password"
                                onChange={handleChange('password')}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={handleMouseDownPassword}
                                            edge="end"
                                        >
                                            {values.showPassword ? <VisibilityOff color="secondary" /> : <Visibility color="secondary" />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </Box>
                    <Box sx={flexBox}>
                        <Box sx={{display:'flex',flexDirection:'row', justifyContent:'center', alignContent:'center'}}>
                            <Checkbox sx={{padding:'0px'}}/>
                            <Typography variant="body2" component="div" alignSelf='center' sx={{margin:'0px'}}>
                                Keep me signed in
                            </Typography>
                        </Box>
                        <Typography variant="body2" component="a" color="#d8b434" alignSelf='center' sx={{fontWeight:"bold",cursor:'pointer'}} onClick={()=>setFormType('forgotPass')}>
                            Forgot Password?
                        </Typography>
                    </Box>

                    <Box>
                        <ColorButton variant="contained" sx={{width:"90%",height:'50px'}} onClick={handleSubmit}>{isLoading ? <CircularProgress /> : "Login"}</ColorButton>
                    </Box>
                    <Box sx={{display:'flex',flexDirection:'row',alignContent:'space-between',justifyContent:'center',margin:'20px 0px'}}>
                        <Typography variant="body2" component="div" alignSelf='center' sx={{fontWeight:"bold"}}>
                            Don't have an account ?
                        </Typography>
                        <Typography variant="body2" component="div" color="#d8b434" alignSelf='center' marginLeft={1} sx={{fontWeight:"bold",cursor:'pointer'}} onClick={()=>setFormType('register1')}>
                            Signup
                        </Typography>
                    </Box>
                </Grid>
            </Box>
        </Grid>
    );
};

export default LoginForm;
