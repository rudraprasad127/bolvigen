import React, {useState} from 'react';
import Typography from "@mui/material/Typography";
import {Grid} from "@mui/material";
import Box from "@mui/material/Box";
import FormControl from "@mui/material/FormControl";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputAdornment from "@mui/material/InputAdornment";
import IconButton from "@mui/material/IconButton";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Visibility from "@mui/icons-material/Visibility";
import {ColorButton} from "./Style";
import {useParams} from "react-router-dom";
import {resatPassword} from "./api";
import Alert from "@mui/material/Alert";
const LoginForm = ({setFormType}) => {
    const [error, setError] = useState('')
    const [values, setValues] = React.useState({
        showPassword1: false,
        showPassword2: false
    });
    const {token} = useParams()
    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const handleSubmit = async () => {
        const finalData = {
            data:{
                password:values.password,
                password_confirmation:values.password_confirmation,
                token
            }
        }
        const {data} = await resatPassword(finalData)
        if(data.errors){
            setError(data.errors)
        }else{
            setFormType('login')
        }
        console.log(data)
    }

    return (
        <Grid item xs={12} className='loginMainDiv'>
            <Box className='loginFormFlex'>
                <Typography variant="h5" component="div" gutterBottom color="purple" sx={{fontWeight:"bold"}}>
                    Reset password
                </Typography>
                <Typography variant="subtitle1" component="div" gutterBottom color="purple" sx={{width:"90%"}}>
                    You have to reset your password
                </Typography>
                <Grid item xs={12} sx={{marginTop:"30px"}}>
                    {
                        error ?
                            error.map((e,index)=>{
                                return(
                                    <Alert key={index} severity="error" sx={{width:'82%',alignSelf:'center'}}>
                                        {e.message}
                                    </Alert>
                                )
                            })
                            : null

                    }
                    <Box>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold",marginBottom:'1'}}>
                            New Password
                        </Typography>
                        <FormControl sx={{ m: 0,width:"90%" }}>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={values.showPassword1 ? 'text' : 'password'}
                                value={values.password}
                                placeholder="Password"
                                onChange={handleChange('password')}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={()=> setValues({
                                                ...values,
                                                showPassword1: !values.showPassword1,
                                            }) }
                                            onMouseDown={handleMouseDownPassword}
                                            edge="end"
                                        >
                                            {values.showPassword1 ? <VisibilityOff color="secondary" /> : <Visibility  color="secondary"/>}
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </Box>
                    <Box sx={{marginTop:'10px'}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold",marginBottom:'1'}}>
                            Confirm Password
                        </Typography>
                        <FormControl sx={{ m: 0,width:"90%" }}>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={values.showPassword2 ? 'text' : 'password'}
                                value={values.password_confirmation}
                                placeholder="Password"
                                onChange={handleChange('password_confirmation')}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={()=> setValues({
                                                ...values,
                                                showPassword2: !values.showPassword2,
                                            }) }
                                            onMouseDown={handleMouseDownPassword}
                                            edge="end"
                                        >
                                            {values.showPassword2 ? <VisibilityOff color="secondary" /> : <Visibility  color="secondary" />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </Box>
                    <Box>
                        <ColorButton variant="contained" color="secondary" sx={{width:"90%",marginBottom:'10px',marginTop:'20px'}} onClick={handleSubmit}>Resat Password</ColorButton>
                    </Box>
                </Grid>
            </Box>
        </Grid>
    );
};

export default LoginForm;
