import React, {useState} from 'react';
import Typography from "@mui/material/Typography";
import {Grid} from "@mui/material";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import {ColorButton} from "./Style";
import {forgotPassword} from "./api";
import Alert from "@mui/material/Alert";
import CircularProgress from "@mui/material/CircularProgress";

const LoginForm = ({setFormType}) => {
    const [values, setValues] = useState({email:''});
    const [isLoading, setIsLoading] = useState(false);
    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    }
    const [error, setError] = useState('')
    const [success, setSuccess] = useState(false);

    const handleSubmit = async () => {
        setIsLoading(true)
        console.log(values.email)
        if(values.email){
            const finalData = {
                data:{
                    attributes:values
                }
            }
            try {
                const {data} = await forgotPassword(finalData)
                if(data.errors){
                    setError(data.errors)
                }else{
                    setSuccess(true)
                }

            }catch (e) {
                setError([
                    {
                        message:'Something Went Wrong'
                    }
                ])
            }
        }else {
            setError([
                {
                    message:'Please Enter Email to continue'
                }
            ])
        }
        setIsLoading(false)
    }

    return (
        <Grid item xs={12} className='loginMainDiv'>
            <Box className='loginFormFlex'>
                <Typography variant="h5" component="div" gutterBottom color="#412360" sx={{fontWeight:"bold"}}>
                    Forgot password?
                </Typography>
                <Typography variant="subtitle2" component="div" gutterBottom color="purple" sx={{width:"90%"}}>
                    Enter your registered email with us, We will send to password reset link on your email address.
                </Typography>
                <Grid item xs={12} sx={{marginTop:"10px"}}>
                    {
                        error ?
                            <Alert severity="error" sx={{width:'82%',alignSelf:'center'}}>
                                {error.map((e)=>{
                                    return(
                                        e.message
                                    )
                                })}
                            </Alert>
                            : null
                    }
                    {
                        success ?
                            <Alert severity="success" sx={{width:'82%',alignSelf:'center'}}>
                                Resat password link sent to your email id.
                            </Alert>
                            : null
                    }
                    <Box sx={{marginBottom:"15px",marginTop:'15px'}}>
                        <Typography variant="subtitle1" component="div" sx={{fontWeight:"bold", marginBottom:'1'}}>
                            Email
                        </Typography>
                        <TextField block placeholder="Email" sx={{width:"90%"}} value={values.email} onChange={handleChange('email')}/>
                    </Box>
                    <Box>
                        <ColorButton variant="contained" color="secondary" sx={{width:"90%",marginBottom:'10px', background: '#633c89', padding : '10px 0 10px 0'}} onClick={handleSubmit}>{isLoading ? <CircularProgress /> : "Verify"}</ColorButton>
                    </Box>
                    <Box>
                        <Button sx={{width:"90%",fontWeight:'bold', color: '#000'}} onClick={() => setFormType('login')}>cancel</Button>
                    </Box>
                </Grid>
            </Box>
        </Grid>
    );
};

export default LoginForm;
