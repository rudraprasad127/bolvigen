import React,{useState,useEffect} from 'react';
import Modal from '@mui/material/Modal';
import {style} from "./Style";
import {Grid} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import {useNavigate, useParams} from 'react-router-dom'
import LoginForm from "./LoginForm";
import ForgotPassword from "./ForgotPassword";
import ResatPassword from "./ResatPassword";
import RegisterStep1 from "./RegisterStep1";
import './style.css'
import SideImage from "./assets/SideImage.png";
import Logo from "./assets/logo.png";
import Cross from "./assets/cross.png";

const Login = () => {
    const {token} = useParams()
    const navigate = useNavigate()
    const [openLoginModel, setOpenLoginModel] = useState(true);
    const [formType, setFormType] = useState('login');
    const handleClose = () => {
        setOpenLoginModel(false)
        setFormType('login')
        navigate('/')
    }
    useEffect(() => {
        if(token){
            console.log(token)
            setFormType('resetPass')
        }
    }, [token]);

    return (
        <div>
            <Modal
                open={openLoginModel}
                aria-labelledby="Login Model"
                aria-describedby="Login Model"
            >
                <Grid className='loginModalMain' sx={style} container>
                    <Grid item xs={0} sm={6} md={6} sx={{ display: { xs: 'none', sm: 'block' } }}>
                        <Box className='loginSideLogo' sx={{backgroundImage:`url(${SideImage})`,backgroundPosition:'right',width: '100%'}}>
                            <Box sx={{padding:'10px',height:'98%',display:'flex',flexDirection:'column',justifyContent:'space-between'}}>
                                <img src={Logo}  width='100px' alt="Brand Logo" style={{ padding:'12px 0 0 15px' }}/>
                                <Typography variant="subtitle2" component="span" sx={{color:'white'}} style={{ padding:'0 0 15px 15px' }}>
                                    GenBov @ 2021. All Rights Reserved.
                                </Typography>
                            </Box>
                        </Box>
                    </Grid>
                    <Grid item xs={0} sm={6} md={6} sx={{width:'100%'}}>
                        <Grid container direction="column" alignContent='flex-end' >
                            <IconButton aria-label="close" onClick={handleClose} style={{ position: 'absolute', right: '10px',top: '5px' }}>
                                <img src={Cross} width='15px' alt="close Button" sx={{padding:'10px'}} />
                            </IconButton>
                        </Grid>

                        {
                            formType === 'login' ? <LoginForm setFormType={setFormType} setOpenLoginModel={setOpenLoginModel} /> : null
                        }
                        {
                            formType === 'forgotPass' ? <ForgotPassword setFormType={setFormType}/> : null
                        }
                        {
                            formType === 'resetPass' ? <ResatPassword setFormType={setFormType}/> : null
                        }
                        {
                            formType === 'register1' ? <RegisterStep1/> : null
                        }
                    </Grid>
                </Grid>
            </Modal>
        </div>
    )
}

export default Login