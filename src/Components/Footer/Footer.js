import React,{Fragment} from 'react';
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import darkLogo from './assets/logoDark.png'
import locationPin from './assets/locationPin.png'
import contact from './assets/Contact.png'
import mail from './assets/mail.png'
const Footer = () => {
    return (
        <Fragment>
            <Box sx={{width: '100%',height:'300px',display:'flex',justifyContent:'center',backgroundColor:'#d6b236',marginTop:'70px'}}>
                <Box sx={{display:'flex',flexDirection:'row',width:'100%',marginTop:'40px'}}>
                    <Box sx={{ flexGrow: 1 }}>

                    </Box>
                    <Box sx={{ flexGrow: 2 , display:'flex',flexDirection:'column',}}>
                        <img src={darkLogo} width='150px' alt="Brand Logo" style={{alignSelf:'left',marginBottom:'20px'}}/>
                        <Box display='flex' alignItems='center' marginBottom={2}>
                            <img src={locationPin} width='20px' alt="Brand Logo" style={{paddingRight:'20px'}}/>
                            <Typography variant="subtitle1" component="div" sx={{ flexGrow: 1,color:'black',cursor:'pointer' }}>
                                Lorem Ipsum is simply Dummy text of the<br/> printing and typesetting industry.
                            </Typography>
                        </Box>
                        <Box display='flex' alignItems='center'  marginBottom={2}>
                            <img src={contact} width='20px' alt="Brand Logo" style={{paddingRight:'20px'}}/>
                            <Typography variant="subtitle1" component="div" sx={{ flexGrow: 1,color:'black',cursor:'pointer' }}>
                                +54 9 358 5109090
                            </Typography>
                        </Box>
                        <Box display='flex' alignItems='center'  marginBottom={2}>
                            <img src={mail} width='20px' alt="Brand Logo" style={{paddingRight:'20px'}}/>
                            <Typography variant="subtitle1" component="div" sx={{ flexGrow: 1,color:'black',cursor:'pointer' }}>
                                xyz@example.com
                            </Typography>
                        </Box>
                    </Box>
                    <Box sx={{ flexGrow: 1 }}>
                        <Typography variant="h5" component="div" sx={{ flexGrow: 1,color:'#6a3a8e',cursor:'pointer',fontWeight:'bold',marginBottom:'20px' }}>
                            Hot Links
                        </Typography>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1,color:'#black',cursor:'pointer',fontWeight:'bold',marginBottom:'10px'}}>
                            Lorem Ipsum
                        </Typography>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1,color:'#black',cursor:'pointer',fontWeight:'bold',marginBottom:'10px'}}>
                            Lorem Ipsum
                        </Typography>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1,color:'#black',cursor:'pointer',fontWeight:'bold',marginBottom:'10px'}}>
                            Lorem Ipsum
                        </Typography>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1,color:'#black',cursor:'pointer',fontWeight:'bold',marginBottom:'10px'}}>
                            Lorem Ipsum
                        </Typography>
                    </Box>
                    <Box sx={{ flexGrow: 1 }}>
                        <Typography variant="h5" component="div" sx={{ flexGrow: 1,color:'#6a3a8e',cursor:'pointer',fontWeight:'bold',marginBottom:'20px'}}>
                            Links
                        </Typography>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1,color:'#black',cursor:'pointer',fontWeight:'bold',marginBottom:'10px'}}>
                            Lorem Ipsum
                        </Typography>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1,color:'#black',cursor:'pointer',fontWeight:'bold',marginBottom:'10px'}}>
                            Lorem Ipsum
                        </Typography>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1,color:'#black',cursor:'pointer',fontWeight:'bold',marginBottom:'10px'}}>
                            Lorem Ipsum
                        </Typography>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1,color:'#black',cursor:'pointer',fontWeight:'bold',marginBottom:'10px'}}>
                            Lorem Ipsum
                        </Typography>
                    </Box>
                    <Box sx={{ flexGrow: 1 }}>
                        <Typography variant="h5" component="div" sx={{ flexGrow: 1,color:'#6a3a8e',cursor:'pointer',fontWeight:'bold',marginBottom:'20px'}}>
                            Useful Links
                        </Typography>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1,color:'#black',cursor:'pointer',fontWeight:'bold',marginBottom:'10px'}}>
                            Lorem Ipsum
                        </Typography>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1,color:'#black',cursor:'pointer',fontWeight:'bold',marginBottom:'10px'}}>
                            Lorem Ipsum
                        </Typography>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1,color:'#black',cursor:'pointer',fontWeight:'bold',marginBottom:'10px'}}>
                            Lorem Ipsum
                        </Typography>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1,color:'#black',cursor:'pointer',fontWeight:'bold',marginBottom:'10px'}}>
                            Lorem Ipsum
                        </Typography>
                    </Box>
                </Box>
            </Box>
            <Box sx={{width: '100%',height:'50px',display:'flex',alignItems:'center',justifyContent:'center',backgroundColor:'#6a3a8e'}}>
                <Typography variant="body" component="span" color="white">
                    © GenBov All rights reserved
                </Typography>
            </Box>
        </Fragment>
    );
};

export default Footer;
