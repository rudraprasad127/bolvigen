import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import darkLogo from './assets/logoDark.png'
import profilePic from './assets/profile.png'
import arrowDown from './assets/arrowDown.png'
import DashboardIcon from '@mui/icons-material/Dashboard';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import PersonIcon from '@mui/icons-material/Person';
import SettingsIcon from '@mui/icons-material/Settings';
import WorkIcon from '@mui/icons-material/Work';
const drawerWidth = 250;


function ResponsiveDrawer(props) {
    const profile = JSON.parse(localStorage.getItem('profile'))
    console.log(profile)
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const { window,Component,name } = props;
    const [mobileOpen, setMobileOpen] = React.useState(false);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };


    const drawer = (
        <div>
            <Toolbar>
                <img width='60%' src={darkLogo} alt={'logo'} />
            </Toolbar>
            <List>
                    <ListItem button>
                        <ListItemIcon>
                            <DashboardIcon sx={{color:'#6a3a8e'}}/>
                        </ListItemIcon>
                        <ListItemText primary={'Dashboard'} sx={{color:'#6a3a8e'}} />
                    </ListItem>
                    <ListItem button>
                        <ListItemIcon>
                            <ShoppingCartIcon sx={{color:'#6a3a8e'}}/>
                        </ListItemIcon>
                        <ListItemText primary={'Orders'} sx={{color:'#6a3a8e'}} />
                    </ListItem>
                    <ListItem button>
                        <ListItemIcon>
                            <WorkIcon  sx={{color:'#6a3a8e'}} />
                        </ListItemIcon>
                        <ListItemText primary={'My Products'} sx={{color:'#6a3a8e'}} />
                    </ListItem>
                <ListItem button>
                    <ListItemIcon>
                        <PersonIcon sx={{color:'#6a3a8e'}}/>
                    </ListItemIcon>
                    <ListItemText primary={'Buyers'} sx={{color:'#6a3a8e'}} />
                </ListItem>
                <ListItem button>
                    <ListItemIcon>
                        <SettingsIcon sx={{color:'#6a3a8e'}}/>
                    </ListItemIcon>
                    <ListItemText primary={'My Profile'} sx={{color:'#6a3a8e'}} />
                </ListItem>
            </List>
        </div>
    );

    const container = window !== undefined ? () => window().document.body : undefined;

    return (
        <Box sx={{ display: 'flex' }}>
            <CssBaseline />
            <AppBar
                position="fixed"
                sx={{
                    width: { sm: `calc(100% - ${drawerWidth}px)` },
                    ml: { sm: `${drawerWidth}px` },
                    backgroundColor:'#f3f3f3',
                    boxShadow:'none',
                    border:'none'

                }}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        sx={{ mr: 2, display: { sm: 'none' } }}
                    >
                        <MenuIcon sx={{backgroundColor:'#6a3a8e'}} />
                    </IconButton>
                    <Box sx={{width:'100%',display:'flex',justifyContent:'space-between'}}>
                        <Typography variant="h4" noWrap component="div" color='#6a3a8e' sx={{fontWeight:'bold'}}>
                            {name}
                        </Typography>
                        <Box
                            sx={{display:'flex',alignItems:'center',cursor:'pointer'}}
                            aria-controls="basic-menu"
                            aria-haspopup="true"
                            aria-expanded={open ? 'true' : undefined}
                            onClick={handleClick}>
                            <img src={profilePic} height='40px' alt='profile pic' sx={{paddingRight:'40px'}}/>
                            <Box id="basic-button" sx={{margin:'0px 10px',display:'flex',flexDirection:'column',cursor:'pointer'}}>
                                <Typography variant="h6" noWrap component="span" color='#6a3a8e' sx={{fontWeight:'bold'}}>
                                    {profile.full_name}
                                </Typography>
                                <Typography variant="caption" noWrap component="span" color='#6a3a8e' marginTop='-7px'>
                                    {profile.user_role.charAt(0).toUpperCase() + profile.user_role.slice(1)}
                                </Typography>
                            </Box>
                            <img src={arrowDown} alt='profile pic'/>
                        </Box>
                    </Box>
                </Toolbar>
                <Menu
                    id="basic-menu"
                    anchorEl={anchorEl}
                    open={open}
                    onClose={handleClose}
                    MenuListProps={{
                        'aria-labelledby': 'basic-button',
                    }}
                >
                    <MenuItem onClick={handleClose}>Profile</MenuItem>
                    <MenuItem onClick={handleClose}>My account</MenuItem>
                    <MenuItem onClick={handleClose}>Logout</MenuItem>
                </Menu>
            </AppBar>
            <Box
                component="nav"
                sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
                aria-label="mailbox folders"
            >
                {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                <Drawer
                    container={container}
                    variant="temporary"
                    open={mobileOpen}
                    onClose={handleDrawerToggle}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                    sx={{
                        display: { xs: 'block', sm: 'none' },
                        '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                    }}
                >
                    {drawer}
                </Drawer>
                <Drawer
                    variant="permanent"
                    sx={{
                        display: { xs: 'none', sm: 'block' },
                        '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                    }}
                    open
                >
                    {drawer}
                </Drawer>
            </Box>
            <Box
                component="main"
                sx={{ flexGrow: 1, p: 3, width: { sm: `calc(100% - ${drawerWidth}px)` } ,backgroundColor:'#f3f3f3'}}
            >
                <Toolbar/>
                <Component/>
            </Box>
        </Box>
    );
}

export default ResponsiveDrawer;
