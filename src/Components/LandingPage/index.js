import React,{Fragment} from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from "@mui/material/Typography";
import {Button} from "@mui/material";
import topBanner from './assets/topBanner.png'
import middleBanner from './assets/middleBanner.png'
import middleBanner2 from './assets/middleBanner2.png'
import halfSizeBanner from './assets/halfSizeBanner.png'
import Image1 from './assets/Image1.png'
import Image2 from './assets/Image2.png'
import Image3 from './assets/Image3.png'
import Image4 from './assets/Image4.png'
import Image5 from './assets/Image5.png'
import Image6 from './assets/Image6.png'
import Image7 from './assets/Image7.png'

const gridStyle = {paddingBottom: "20px"}

const LandingPage = () => {

    return (
        <Fragment>
            <Grid container sx={{width: '100%',height:'450px',backgroundImage:`url(${topBanner})`,backgroundPosition: 'center',display:'flex'}}>
                <Grid item xs={12} sm={6} sx={{display:'flex',flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                    <Box>
                        <Typography variant="h2" component="div" color="white" sx={{fontWeight:"bold",marginBottom:'30px'}}>
                            Title Heading
                        </Typography>
                        <Typography variant="h6" component="div" color="white" sx={{fontWeight:"bold",marginBottom:'30px'}}>
                            Lorem Ipsum is simply Dummy text of the<br/> printing and typesetting industry.
                        </Typography>
                        <Button style={{width:'150px',height:'50px',backgroundColor:'#d6b236',color:'black',borderRadius:'5px',fontWeight:'bold'}} >Show More</Button>
                    </Box>
                </Grid>
                <Grid item sx={0} sm={6}>
                </Grid>
            </Grid >
            <Box  sx={{width: '90%',height:'100%',display:'flex',flexDirection:'center',alignItems:'center',alignContent:'center',justifyContent:'center', paddingTop: '50px', paddingBottom: '50px', margin : '10px auto'}}>
                <Grid container xs={12} sx={{display:'flex'}} spacing={0}>
                    <Grid item xs={12} sm={12} md={6} sx={{width:'100%',display:'flex',justifyContent:'center',flexDirection:'row', padding:"10px 5px 0px 10px"}}>
                        <Box sx={{width:'100%',backgroundImage:`url(${Image1})`,backgroundPosition: 'center',backgroundSize:'cover',display:'flex',borderRadius:'10px', margin: '0 auto'}}>
                            <Typography variant="h4" component="div" color="white" sx={{fontWeight:"bold",margin:'30px',alignSelf:'flex-end'}}>
                                BRAFORD
                            </Typography>
                        </Box>
                    </Grid>
                    <Grid item xs={12} sm={12} md={6} sx={{width:'100%',display:'flex',flexDirection:'row',padding:"8px 10px 0px 10px"}}>
                        <Grid container columns={{ xs: 6, sm: 6, md: 6 }} columnSpacing={3} sx={{width:'100%',display:'flex', margin: '0 auto'}}>
                            <Grid item xs={3} sm={3} md={3} sx={gridStyle}>
                                <Box sx={{width:'100%',height:'240px',backgroundImage:`url(${Image2})`,backgroundPosition: 'center',backgroundSize:'cover',display:'flex',borderRadius:'10px'}}>
                                    <Typography variant="h4" component="div" color="white" sx={{fontWeight:"bold",margin:'20px',alignSelf:'flex-end'}}>
                                        ANGUS
                                    </Typography>
                                </Box>
                            </Grid>
                            <Grid item xs={3} sm={3} md={3} sx={gridStyle}>
                                <Box sx={{width:'100%',height:'240px',backgroundImage:`url(${Image3})`,backgroundPosition: 'center',backgroundSize:'cover',display:'flex',borderRadius:'10px'}}>
                                    <Typography variant="h4" component="div" color="white" sx={{fontWeight:"bold",margin:'20px',alignSelf:'flex-end'}}>
                                        HEREFORD
                                    </Typography>
                                </Box>
                            </Grid>
                            <Grid item xs={3} sm={3} md={3} >
                                <Box sx={{width:'100%',height:'240px',backgroundImage:`url(${Image4})`,backgroundPosition: 'center',backgroundSize:'cover',display:'flex',borderRadius:'10px'}}>
                                    <Typography variant="h4" component="div" color="white" sx={{fontWeight:"bold",margin:'20px',alignSelf:'flex-end'}}>
                                        BRANGUS
                                    </Typography>
                                </Box>
                            </Grid>
                            <Grid item xs={3} sm={3} md={3}>
                                <Box sx={{width:'100%',height:'240px',backgroundImage:`url(${Image5})`,backgroundPosition: 'center',backgroundSize:'cover',display:'flex',borderRadius:'10px'}}>
                                    <Typography variant="h4" component="div" color="white" sx={{fontWeight:"bold",margin:'20px',alignSelf:'flex-end'}}>
                                        Limangus
                                    </Typography>
                                </Box>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Box>
            <Box sx={{width: '100%',height:'450px',backgroundImage:`url(${middleBanner})`,backgroundPosition: 'center',backgroundSize:'cover',display:'flex'}}>
                <Grid item xs={0} sm={7} >

                </Grid>
                <Grid item xs={12} sm={5} sx={{display:'flex',flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                    <Box sx={{display:'flex',flexDirection:'column',alignItems:'flex-end'}}>
                        <Typography variant="h2" component="div" color="#6a3a8e" sx={{fontWeight:"bold",marginBottom:'30px'}}>
                            Title Heading
                        </Typography>
                        <Typography variant="h6" component="div" color="#6a3a8e" sx={{fontWeight:"bold",marginBottom:'30px',textAlign:'right'}}>
                            Lorem Ipsum is simply Dummy text of the<br/> printing and typesetting industry.
                        </Typography>
                        <Button style={{width:'150px',height:'50px',backgroundColor:'#d6b236',color:'black',borderRadius:'5px',fontWeight:'bold'}} >Show More</Button>
                    </Box>
                </Grid>
            </Box>
            <Box sx={{width: '100%',display:'flex',flexDirection:'center',alignItems:'center',alignContent:'end',justifyContent:'space-around', padding : '45px 0'}}>
                <Grid container spacing={2} sx={{display:'flex',flexDirection:'row',alignItems:'space-around',justifyContent:'space-around'}}>
                    <Grid item xs={12} sm={6} sx={{height:'490px',width:'100%',display:'flex',justifyContent:'end'}}>
                        <Box sx={{display:'flex',flexDirection:'column',alignItems:'center'}}>
                            <img src={Image6} width='90%' alt="landing page"/>
                            <Box sx={{width:'90%',height:'20px',backgroundColor:'#d6b236',margin:'0', marginTop:'-4px'}}>
                            </Box>
                            <Box sx={{width:'90%',height:'100px',backgroundColor:'#6a3a8e',margin:'0',display:'flex',flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                                <Typography variant="h4" component="div" color="white" sx={{fontWeight:"bold",marginLeft:'30px'}}>
                                    HEREFORD
                                </Typography>
                                <Button style={{width:'150px',height:'50px',backgroundColor:'#d6b236',color:'black',borderRadius:'5px',fontWeight:'bold',marginRight:'30px'}} >Show More</Button>
                            </Box>
                        </Box>
                    </Grid>
                    <Grid item xs={12} sm={6} sx={{height:'490px',width:'100%',display:'flex'}}>
                        <Box >
                            <Box sx={{display:'flex',flexDirection:'column',alignItems:'center'}}>
                                <img src={Image7} width='90%' alt="landing page"/>
                                <Box sx={{width:'90%',height:'20px',backgroundColor:'#d6b236',marginTop:'-4px'}}>
                                </Box>
                                <Box sx={{width:'90%',height:'100px',backgroundColor:'#6a3a8e',margin:'0',display:'flex',flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                                    <Typography variant="h4" component="div" color="white" sx={{fontWeight:"bold",marginLeft:'30px'}}>
                                        HEREFORD
                                    </Typography>
                                    <Button style={{width:'150px',height:'50px',backgroundColor:'#d6b236',color:'black',borderRadius:'5px',fontWeight:'bold',marginRight:'30px'}} >Show More</Button>
                                </Box>
                            </Box>
                        </Box>
                    </Grid>
                </Grid>
            </Box>
            <Box sx={{width: '100%',height:'500px',backgroundImage:`url(${middleBanner2})`,backgroundSize:'cover',display:'flex'}}>
                <Grid item xs={12} sx={{width:'100%',display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                    <Typography variant="h2" component="div" color="white" sx={{fontWeight:"bold",marginBottom:'30px'}}>
                        Title Heading
                    </Typography>
                    <Button style={{width:'150px',height:'50px',backgroundColor:'#d6b236',color:'black',borderRadius:'5px',fontWeight:'bold'}} >Show More</Button>
                </Grid>
            </Box>
            <Box  sx={{width: '100%',height:'450px',display:'flex',justifyContent:'center'}}>
                <Grid item sx={{width: '90%',height:'450px',display:'flex',margin:'30px',borderRadius:'20px',backgroundColor:'#6a3a8e'}}>
                    <Grid item xs={12} sm={6} sx={{backgroundImage:`url(${halfSizeBanner})`,backgroundSize:'cover',borderRadius:'20px 0px 0px 20px;'}} >
                    </Grid>
                    <Box  sx={{width:'10px',backgroundColor:'#d6b236'}}>

                    </Box>
                    <Grid item xs={12} sm={6} sx={{display:'flex',flexDirection:'row',justifyContent:'center',alignItems:'center',}}>
                        <Box sx={{display:'flex',flexDirection:'column',alignItems:'Center',}}>
                            <Typography variant="h2" component="div" color="white" sx={{fontWeight:"bold",marginBottom:'30px'}}>
                                Title Heading
                            </Typography>
                            <Button style={{width:'150px',height:'50px',backgroundColor:'#d6b236',color:'black',borderRadius:'5px',fontWeight:'bold'}} >Show More</Button>
                        </Box>
                    </Grid>
                </Grid>
            </Box>
        </Fragment>
    );
}

export default LandingPage;
