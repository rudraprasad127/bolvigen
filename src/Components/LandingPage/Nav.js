import React,{useState,useEffect} from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import {Button} from "@mui/material";
import {useNavigate} from 'react-router-dom'
import Logo from "./assets/logo.png";
import cart from "./assets/cart.png";
import bell from "./assets/bell.png";
import search from "./assets/search.png";


// eslint-disable-next-line no-undef
export default function Nav() {
    const navigate = useNavigate()
    const [auth, setAuth] = useState('');
    // added by ss
    const token = localStorage.getItem('authToken')
    useEffect(() => {
        if(token){
            setAuth(token)
        }
    }, [auth,token]);

    const handleNavigation = () => {
        if(auth){
            localStorage.removeItem('authToken')
            localStorage.removeItem('profile')
            setAuth('')
        }else{
            navigate('/login')
        }
    }
    // const list = (anchor) => (
    //     <Box
    //     style={{"width" : "100%", minWidth: '300px'}}
    //       role="presentation"
    //       onClick={toggleDrawer(anchor, false)}
    //       onKeyDown={toggleDrawer(anchor, false)}
    //     >
    //       <List>
    //         {['Menu 1', 'Menu 2', 'Menu 3', 'Menu 4', 'Menu 5'].map((text, index) => (
    //           <ListItem button key={text}>
    //             <ListItemText primary={text} style={{ padding : '10px'}} />
    //           </ListItem>
    //         ))}
    //       </List>
    //     </Box>
    //   );

    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static" style={{backgroundColor:'#6a3a8e'}}>
                <Toolbar>
                    <Box sx={{ flexGrow: 1 }}>
                        <img src={Logo} width='100px' alt="Brand Logo" style={{padding:'10px'}}/>
                    </Box>
                    <img src={search} width='20px' alt="Brand Logo" style={{marginRight:'20px',cursor:'pointer'}}/>
                    <img src={cart} width='20px' alt="Brand Logo" style={{marginRight:'20px',cursor:'pointer'}}/>
                    <img src={bell} width='20px' alt="Brand Logo"    style={{marginRight:'20px',cursor:'pointer'}}/>
                    <Button style={{width:'100px',backgroundColor:'#d6b236',color:'black',border:'2px solid white',borderRadius:'5px',fontWeight:'bold'}} onClick={handleNavigation}>{auth ? 'Logout' : 'Login'}</Button>
                </Toolbar>
            </AppBar>
            <AppBar position="static" style={{backgroundColor:'#d6b236'}}>
                <Toolbar>
                    <Box sx={{ flexGrow: 1 }}>
                    </Box>
                    <Box sx={{ flexGrow: 1 }}>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1,color:'black',cursor:'pointer' }}>
                            Menu 2
                        </Typography>
                    </Box>
                    <Box sx={{ flexGrow: 1 }}>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1,color:'black',cursor:'pointer' }}>
                            Menu 2
                        </Typography>
                    </Box>
                    <Box sx={{ flexGrow: 1 }}>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1,color:'black',cursor:'pointer' }}>
                            Menu 3
                        </Typography>
                    </Box>
                    <Box sx={{ flexGrow: 1 }}>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1,color:'black',cursor:'pointer' }}>
                            Menu 4
                        </Typography>
                    </Box>
                    <Box sx={{ flexGrow: 1 }}>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1,color:'black',cursor:'pointer' }}>
                            Menu 5
                        </Typography>
                    </Box>
                </Toolbar>
            </AppBar>

        </Box>

    );
}